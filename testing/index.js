
require( "./lib/imports" );

require( "../lib/imports" );

global.verbose = false;

function performUnitTests() {
	return new Promise( async ( resolve, reject )=> {

		var controller = new tests.unit.classes.base.Controller();
		controller.testExtractLines();
		controller.testProcessLines();
		controller.testProcessLine();
		controller.testProcessAssignments();
		controller.testProcessValueLine();
		controller.testProcessMetalLine();
		controller.testProcessQuestions();

		var symbol = new tests.unit.classes.base.Symbol();
		symbol.testIsSubtractable();

		var answer = new tests.unit.classes.Answer();
		answer.testCalculateResult();

		var parser = new tests.unit.utils.Parser();
		parser.testParseLine();
	} );
};

function performIntegrationTests() {
	// NOTHING TO TEST HERE
};

function performEndToEndTests() {
	// NOTHING TO TEST HERE
};

async function main() {
	await performUnitTests();
	//await performIntegrationTests();
	//await performEndToEndTests();
};

main();