
const tape = require( "tape" );

class Controller {
	testExtractLines() {
		return new Promise( ( resolve, reject )=> {
			tape( "Extract Lines", ( assert )=> {
				var controllerInstance = new classes.base.Controller();
				var input = "glob is I\r\nprok is V" ;
				var extractionResult = controllerInstance.extractLines( input );

				assert.equal( Array.isArray( extractionResult ), true, "Result is of type Array" );
				assert.deepEqual( extractionResult, [ "glob is I", "prok is V" ], "Array.split on \\r\\n" );

				input = "glob is I\nprok is V";
				extractionResult = controllerInstance.extractLines( input );
				assert.deepEqual( extractionResult, [ "glob is I", "prok is V" ], "Array.split on \\n - without carriage return" );

				assert.throws( ()=> { controllerInstance.extractLines( undefined ) }, TypeError, "Should throw TypeError" );

				resolve( assert.end() );
			} );
		} );
	};

	testProcessLines() {
		return new Promise( ( resolve, reject )=> {
			tape( "Process Lines", ( assert )=> {
				var controllerInstance = new classes.base.Controller();
				var lines = [ "glob is I", "glob glob Silver is 34 Credits", "how much is pish tegj glob glob ?", "" ];
				controllerInstance.processLines( lines );

				assert.equal( controllerInstance.assignments.length, 2, "Two assignments were made" );
				assert.equal( controllerInstance.questions.length, 1, "One question was asked" );
				assert.equal( controllerInstance.answers[ 0 ].outputLine, "I have no idea what you are talking about", "The system does not understand the question" );

				assert.throws( ()=> { controllerInstance.processLines( [ "" ] ) }, TypeError, "Should throw TypeError" );

				resolve( assert.end() );
			} );
		} );
	};

	testProcessLine() {
		return new Promise( ( resolve, reject )=> {
			tape( "Process Line", ( assert )=> {
				var controllerInstance = new classes.base.Controller();
				var line_one = "glob is I";
				var line_two = "how much is pish tegj glob glob ?";
				controllerInstance.processLine( line_one, new utils.Parser() );
				controllerInstance.processLine( line_two, new utils.Parser() );

				assert.equal( controllerInstance.assignments[ 0 ].type, "ASSIGNMENT", "Line TYPE is ASSIGNMENT" );
				assert.equal( controllerInstance.questions[ 0 ].type, "QUESTION", "Line TYPE is QUESTION" );

				assert.throws( ()=> { controllerInstance.processLine( line_one ) }, TypeError, "Should throw TypeError" );

				resolve( assert.end() );
			} );
		} );
	};

	testProcessAssignments() {
		return new Promise( ( resolve, reject )=> {
			tape( "Process Assignments", ( assert )=> {
				var controllerInstance = new classes.base.Controller();
				controllerInstance.assignments.push( new classes.Assignment( "glob is I" ) );
				controllerInstance.assignments.push( new classes.Assignment( "pish is X" ) );
				controllerInstance.assignments.push( new classes.Assignment( "glob Silver is 5 Credits" ) );
				controllerInstance.processAssignments();

				var symbols = controllerInstance.symbols.filter( ( symbol )=> ( symbol.pricePerUnit === undefined ) );
				var metals = controllerInstance.symbols.filter( ( symbol )=> ( symbol.pricePerUnit !== undefined ) );

				assert.equal( symbols.length, 2, "Two SYMBOL assignments were made" );
				assert.equal( metals.length, 1, "One METAL assignment was made" );

				assert.equal( metals[ 0 ].pricePerUnit, 5, "The PPU for Silver should be 5" );

				var controllerInstance = new classes.base.Controller();
				controllerInstance.processAssignments();

				assert.equal( controllerInstance.symbols.length, 0, "Empty assignments means EMPTY symbols" );

				resolve( assert.end() );
			} );
		} );
	};

	testProcessValueLine() {
		return new Promise( ( resolve, reject )=> {
			tape( "Process Value Line", ( assert )=> {
				var controllerInstance = new classes.base.Controller();
				controllerInstance.processValueLine( new classes.Assignment( "glob is I" ), { romanNumeral: "I", arabicNumeral: 1 } );

				assert.equal( controllerInstance.symbols.length, 1, "One value Symbol was added" );
				assert.equal( controllerInstance.symbols[ 0 ].identifier, "glob", "Symbol identifier is 'glob'" );
				assert.equal( controllerInstance.symbols[ 0 ].value.romanNumeral, "I", "Symbol value-assignment equates to Roman 'V' numeral" );

				assert.throws( ()=> { controllerInstance.processValueLine( new classes.Assignment( "glob is I" ) ) }, TypeError, "Should throw TypeError" );
				assert.throws( ()=> { controllerInstance.processValueLine( undefined, { romanNumeral: "I", arabicNumeral: 1 } ) }, TypeError, "Should throw TypeError" );

				resolve( assert.end() );
			} );
		} );
	};

	testProcessMetalLine() {
		return new Promise( ( resolve, reject )=> {
			tape( "Process Metal Line", ( assert )=> {
				var controllerInstance = new classes.base.Controller();
				controllerInstance.processValueLine( new classes.Assignment( "glob is I" ), { romanNumeral: "I", arabicNumeral: 1 } );
				controllerInstance.processMetalLine( new classes.Assignment( "glob glob Silver is 34 Credits" ), { romanNumeral: "I", arabicNumeral: 1 } );

				assert.equal( controllerInstance.symbols.length, 2, "Two Symbols were added" );

				var metalSymbol = controllerInstance.symbols.find( ( symbol )=> ( symbol.pricePerUnit !== undefined ) );
				assert.equal( parseInt( metalSymbol.value ), parseInt( metalSymbol.pricePerUnit * metalSymbol.amount ), "The amount of metal should be the value / PPU" );

				resolve( assert.end() );
			} );
		} );
	};

	testProcessQuestions() {
		return new Promise( ( resolve, reject )=> {
			tape( "Process Questions", ( assert )=> {
				var controllerInstance = new classes.base.Controller();
				var lines = [ "glob is I", "glob glob Silver is 34 Credits" ];
				controllerInstance.processLines( lines );

				assert.equal( controllerInstance.questions.length, 0, "No QUESTIONS were given" );

				controllerInstance.questions.push( new classes.Question( "how many Credits is glob glob Silver ?" ) );
				controllerInstance.processQuestions();
				assert.equal( controllerInstance.answers.length, 1, "One question means one answer" );

				assert.equal( controllerInstance.answers[ 0 ].outputLine, "glob glob Silver is 34 Credits", "The symbols for the question WAS specified" );

				var controllerInstance = new classes.base.Controller();
				var lines = [ "glob is I", "glob glob Silver is 34 Credits" ];
				controllerInstance.processLines( lines );

				controllerInstance.questions.push( new classes.Question( "how many Credits is glob prok Silver ?" ) );
				controllerInstance.processQuestions();

				assert.equal( controllerInstance.answers[ 0 ].outputLine, "I have no idea what you are talking about", "Unknown symbols in question" );

				resolve( assert.end() );
			} );
		} );
	};
};

module.exports = Controller;