
const tape = require( "tape" );

class Symbol {
	testIsSubtractable() {
		return new Promise( ( resolve, reject )=> {
			tape( "Is Subtractable", ( assert )=> {
				var symbolInstance = new classes.base.Symbol( "bleh", { romanNumeral: "I", arabicNumeral: 1 } );
				var sibling = { value: { romanNumeral: "V", arabicNumeral: 5 } };

				var subtractable = symbolInstance.isSubtractable( sibling );
				assert.equal( subtractable, true, "I is subtractable from V" );

				symbolInstance = new classes.base.Symbol( "bleh", { romanNumeral: "V", arabicNumeral: 5 } );
				sibling = { value: { romanNumeral: "I", arabicNumeral: 1 } };

				subtractable = symbolInstance.isSubtractable( sibling );
				assert.equal( subtractable, false, "V is NOT subtractable from I" );

				symbolInstance = new classes.base.Symbol( "bleh", { romanNumeral: "I", arabicNumeral: 1 } );
				sibling = { value: { romanNumeral: "L", arabicNumeral: 50 } };

				subtractable = symbolInstance.isSubtractable( sibling );
				assert.equal( subtractable, false, "I is NOT subtractable from L" );

				symbolInstance = new classes.base.Symbol( "bleh", { romanNumeral: "I", arabicNumeral: 1 } );
				sibling = { value: { romanNumeral: "M", arabicNumeral: 1000 } };

				subtractable = symbolInstance.isSubtractable( sibling );
				assert.equal( subtractable, false, "I is NOT subtractable from M" );

				subtractable = symbolInstance.isSubtractable( undefined );
				assert.equal( subtractable, false, "Without a sibling, nothing can be subtractable" );

				resolve( assert.end() );
			} );
		} );
	};
};

module.exports = Symbol;