
const tape = require( "tape" );

class Answer {
	testCalculateResult() {
		return new Promise( ( resolve, reject )=> {
			tape( "Calculate Result", ( assert )=> {
				var answerInstance = new classes.Answer();
				answerInstance.symbols.push( new classes.base.Symbol( "glob", { romanNumeral: "I", arabicNumeral: 1 } ) );
				answerInstance.symbols.push( new classes.base.Symbol( "glob", { romanNumeral: "I", arabicNumeral: 1 } ) );

				var metalSymbol = new classes.Mineral( "Silver", 2 );
				metalSymbol.pricePerUnit = 1;
				metalSymbol.amount = 2;

				answerInstance.symbols.push( metalSymbol );
				answerInstance.calculateResult();

				assert.equal( answerInstance.outputLine, "glob glob Silver is 2 Credits", "Case is designed for 2 Silvers at 1 Credit each..." );

				answerInstance = new classes.Answer();
				answerInstance.symbols.push( new classes.base.Symbol( "glob", { romanNumeral: "I", arabicNumeral: 1 } ) );
				answerInstance.calculateResult();
				assert.equal( answerInstance.outputLine, "glob is 1", "System functions without metal-definitions" );

				answerInstance = new classes.Answer();
				answerInstance.symbols.push( "" );
				answerInstance.calculateResult();

				assert.equal( answerInstance.outputLine, "I have no idea what you are talking about", "Without valid assignment symbols, the system can't know what is wanted." );

				resolve( assert.end() );
			} );
		} );
	};
};

module.exports = Answer;