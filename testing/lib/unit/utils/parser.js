
const tape = require( "tape" );

class Parser {
	testParseLine() {
		return new Promise( ( resolve, reject )=> {
			tape( "Parse Line", ( assert )=> {
				var parserInstance = new utils.Parser();

				var line_one = "line is assignment";
				var line_two = "line_one is an assignment ?";

				var parsedLine = parserInstance.parseLine( line_one );
				assert.equal( parsedLine.type, "ASSIGNMENT", "Line One is an assignment" );

				parsedLine = parserInstance.parseLine( line_two );
				assert.equal( parsedLine.type, "QUESTION", "Line Two is a question" );

				assert.throws( ()=> { parserInstance.parseLine( undefined ) }, TypeError, "Should throw TypeError" );

				resolve( assert.end() );
			} );
		} );
	};
};

module.exports = Parser;