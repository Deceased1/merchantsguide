
global.tests = { unit: { classes: { base: {} }, controllers: {}, utils: {} } };

tests.unit.classes.base.Controller = require( "./unit/classes/base/controller" );
tests.unit.classes.base.Symbol = require( "./unit/classes/base/symbol" );
tests.unit.classes.Answer = require( "./unit/classes/answer" );

tests.unit.utils.Parser = require( "./unit/utils/parser" );