
require( "./lib/imports" );

const mode = process.argv[ 2 ];
const target = process.argv[ 3 ];

global.verbose = false;

/*
 * Initiates the application in "Interactive" mode, where 
 *     input-lines are typed by hand into the terminal.
 */
async function processInput() {
	var interactiveController = new controllers.InteractiveController();
	var input = await interactiveController.processInput();
	var lines = interactiveController.extractLines( input );
	var processedLines = interactiveController.processLines( lines );
	interactiveController.answers.forEach( ( answer )=> {
		console.log( answer.outputLine );
	} );
};

/*
 * Initiates the application in "Line" mode, where
 *     input is typed inline in a single line.
 */
function processLine( lineString ) {
	var lineController = new controllers.LineController();
	var lines = lineController.extractLines( lineString.toString() );
	var processedLines = lineController.processLines( lines );
	lineController.answers.forEach( ( answer )=> {
		console.log( answer.outputLine );
	} );
};

/*
 * Initiates the application in "File" mode, where
 *     input is read from file.
 */
async function processFile( filePath ) {
	var fileController = new controllers.FileController();
	var fileData = await fileController.readFile( filePath );
	var lines = fileController.extractLines( fileData );
	var processedLines = fileController.processLines( lines );
	fileController.answers.forEach( ( answer )=> {
		console.log( answer.outputLine );
	} );
};

/*
 * Displays the help menu.
 */
function help( mode, target ) {
	if ( mode === null || mode === undefined ) {
		console.log( "Valid modes: " );
		console.log( "\tinteractive" );
		console.log( "\tfile" );
		console.log( "\tline" );
	}

	if ( mode === "help" ) {
		switch( target ) {
			case "interactive": console.log( "Application enters 'Interactive' mode - blank newline signals end-of-input" ); break;
			case "file": console.log( "Application enters 'File' mode - input is specified by file-location" ); break;
			case "line": console.log( "Application enters 'Line' mode - input is given inline by parameter" ); break;
		}
	}

	process.exit( 0 );
};

function main( mode, target ) {
	if ( ( mode === void 0 ) ) {
		help( mode, target );
	}

	switch ( mode ) {
		case "interactive": processInput(); break;
		case "file": processFile( target ); break;
		case "line": processLine( target ); break;
		default: help( mode, target );
	}
};

main( mode, target );