
const path_module = require( "path" );

const { spawn } = require( "child_process" );

class FileController extends classes.base.Controller {
	constructor() {
		super();
	};

	/*
	 * Spawns a new child-process which will read the file at the path specified.
	 *     This function performs asynchronously, only resolving once the file-reader is done.
	 * filePath: {String} A path on the filesystem where the target-file resides.
	 */
	readFile( filePath ) {
		if ( filePath === null || filePath === undefined ) {
			throw new TypeError( "filePath parameter is undefined" );
		}
		return new Promise( async ( resolve, reject )=> {
			try {
				var fileReaderPath = path_module.resolve( __dirname, "../utils/fileReader.js" );
				var result = "";

				var child = spawn( "node", [ fileReaderPath, filePath ] );

				child.stderr.on( "data", ( error )=> { reject( error.toString() ); } );
				child.stdout.on( "data", ( data )=> { result += data.toString( "utf-8" ); } );
				child.on( "close", ( code )=> {
					( code !== 0 ) ? reject( `Process 'fileReader.js' ended with code: ${code}` ) : resolve( result );
				} );
			} catch( err ) {
				reject( err );
			}
		} );
	};
};

module.exports = FileController;