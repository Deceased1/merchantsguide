
const readline_module = require( "readline" );

class InteractiveController extends classes.base.Controller {
	constructor() {
		super();
	};

	async processInput() {
		return await this.startLineReader();
	};

	/*
	 * Reads user-input from the stdin stream and will not end until an empty-newline is found.
	 *     Once the end-of-input is reached, the input is returned as a continuous string.
	 * return {String} A string containing all the characters the user provided.
	 */
	startLineReader() {
		return new Promise( ( resolve, reject )=> {
			var lineReader = readline_module.createInterface( { input: process.stdin } );
			var newLineCount = 0;
			var result = "";

			lineReader.on( "line", ( input )=> {
				if ( input === "" ) {
					newLineCount++;
					if ( newLineCount > 0 ) {
						lineReader.close();
					}
				} else {
					result += input + "\r\n";
					newLineCount = 0;
				}
			} );

			lineReader.on( "close", ()=> {
				resolve( result );
			} );
		} );
	};
};

module.exports = InteractiveController;