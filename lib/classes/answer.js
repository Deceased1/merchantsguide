
class Answer {
	constructor() {
		this.symbols = [];
		this.outputLine = "";
	};

	/*
	 * Separates the symbols, iterates over the non-metal symbols, determining the amount specified by the question,
	 *     then multiplies said amount with the metal-symbols' "price-per-unit" field, which results in the answer to the question.
	 */
	calculateResult() {
		try {
			var amount = 0;
			var mineralSymbol = this.symbols.find( ( symbol )=> ( symbol.pricePerUnit !== undefined ) );
			var amountSymbols = this.symbols.filter( ( symbol )=> ( symbol.pricePerUnit === undefined ) );
			amountSymbols.forEach( ( symbol, index )=> {
				/*
				 * If the symbol cannot be determined ( undefined ), performing any further actions against it would
				 *     result in a throw, which is ideal for when the system does not understand the question.
				 */
				if ( symbol !== undefined ) {
					this.outputLine += symbol.identifier + " ";
					if ( symbol.pricePerUnit === undefined ) {
						if ( symbol.isSubtractable( this.symbols[ index + 1 ] ) ) {
							amount += ( amount - symbol.value.arabicNumeral );
						} else {
							amount += symbol.value.arabicNumeral;
						}
					}
				}
			} );

			if ( mineralSymbol !== null && mineralSymbol !== undefined ) {
				this.outputLine += mineralSymbol.identifier + " is " + ( mineralSymbol.pricePerUnit * amount ) + " Credits";
			} else {
				this.outputLine += "is " + amount;
			}
		} catch( err ) {
			this.outputLine = "I have no idea what you are talking about";
		}
	};
};

module.exports = Answer;