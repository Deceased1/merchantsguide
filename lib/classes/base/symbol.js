
/* NOTE:
 * "I" can be subtracted from "V" and "X".
 * "X" can be subtracted from "L" and "C".
 * "C" can be subtracted from "D" and "M".
 */

class Symbol {
	constructor( identifier, value ) {
		this.identifier = identifier;
		this.value = value;
	};

	/*
	 * Determines whether a symbol should be subtracted from its sibling-symbol.
	 * sibling: {Object} The sibling ConversionDefinition object.
	 */
	isSubtractable( sibling ) {
		if ( sibling === null || sibling === undefined ) {
			return false;
		}

		if ( this.value.romanNumeral === "I" ) {
			if ( ( sibling.value.romanNumeral === "V" ) || ( sibling.value.romanNumeral === "X" ) ) {
				return true;
			}
		} else if ( this.value.romanNumeral === "X" ) {
			if ( ( sibling.value.romanNumeral === "L" ) || ( sibling.value.romanNumeral === "C" ) ) {
				return true;
			}
		} else if ( this.value.romanNumeral === "C" ) {
			if ( ( sibling.value.romanNumeral === "D" ) || ( sibling.value.romanNumeral === "M" ) ) {
				return true;
			}
		}
		return false;
	};
};

module.exports = Symbol;