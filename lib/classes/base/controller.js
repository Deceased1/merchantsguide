
class Controller {
	constructor() {
		this.assignments = [];
		this.symbols = [];
		this.questions = [];
		this.answers = [];
	};

	/*
	 * Splits lines on:
	 *     carriage-return and newline OR newline without a preceding carriage-return.
	 * lines: {String} The string to be split up into an array of strings.
	 * return {Array<String>} Returns an array of strings.
	 */
	extractLines( lines ) {
		if ( lines === null || lines === undefined ) {
			throw new TypeError( "lineString parameter is undefined" );
		}

		return lines.split( /\r?\n/ );
	};

	/*
	 * Runs the lines through the parser, one-by-one, resulting this objects'
	 *     "assignments" and "questions" variables to be filled.
	 *     These are then processed accordingly.
	 * lines: {Array<String>} An array of strings to be processed.
	 */
	processLines( lines ) {
		if ( Array.isArray( lines ) === false ) {
			throw new TypeError( "lines parameter is not of type Array" );
		}

		var parser = new utils.Parser();
		lines.forEach( ( line )=> {
			/* Seeing as a blank-newline dictates the end-of-input, we do not process lines with no content */
			if ( line !== "" ) {
				this.processLine( line, parser );
			}
		} );

		this.processAssignments();
		this.processQuestions();
	};

	/*
	 * Runs a given line through a given instance of the utils.Parser class.
	 *     Once the line has been parsed, based on the resulting object, the line is either added
	 *     to this objects' "assignments" or "questions" variable.
	 * line: {String} A string provided as input to the application.
	 * parser: {utils.Parser} An instance of the utils.Parser class.
	 */
	processLine( line, parser ) {
		if ( parser === null || parser === undefined ) {
			throw new TypeError( "parser parameter is undefined" );
		}

		var parsedLine = parser.parseLine( line );
		if ( parsedLine.type === "ASSIGNMENT" ) {
			this.assignments.push( parsedLine );
		} else {
			this.questions.push( parsedLine );
		}
	};

	/*
	 * Iterates though this objects' "assignments" variable and processes each assignment-instance depending on whether
	 *     the assignment is attempting to assign data to a symbol or a metal.
	 */
	processAssignments() {
		this.assignments.forEach( ( assignment )=> {
			var conversion = definitions.ConversionDefinition.find( ( definition )=> ( definition.romanNumeral === assignment.data.slice( -1 ) ) );

			/*
			 * If the line-data contains a conversion, such as "X" in the case: "pish is X",
			 *     it should be safe to assume that we are assigning a roman-numeral/arabic-numeral pair to a symbol
			 */
			if ( conversion !== null && conversion !== undefined ) {
				this.processValueLine( assignment, conversion );
			} 
			/*
			 * If the line-data contains no coversion, it should be safe to assume that we are
			 *     assigning symbols to minerals, which means the line dictates a price of a specific amount of
			 *     metals, such as "pish pish Iron is 3910 Credits".
			 */
			else {
				this.processMetalLine( assignment, conversion );
			}
		} );
	};

	/*
	 * Breaks down the data provided by an assignment, dynamically allocating a value to
	 *     a symbol, based on the original data provided by the input-line.
	 * assignment: {classes.Assignment} An instance of the classes.Assignment class.
	 * conversion: {Object} The corresponding definition, containing the numerables and identifiers for the assignment.
	 */
	processValueLine( assignment, conversion ) {
		if ( assignment === null || assignment === undefined ) {
			throw new TypeError( "assignment parameter is undefined" );
		}
		if ( conversion === null || conversion === undefined ) {
			throw new TypeError( "conversion parameter is undefined" );
		}
		
		var identifier = assignment.data.substring( 0, assignment.data.indexOf( " is" ) );
		var value = conversion;
		var symbol = new classes.base.Symbol( identifier, value );
		this.symbols.push( symbol );
	};

	/*
	 * Breaks down the data provided by an assignment, dynamically determining the "price-per-unit" for
	 *     any specified metal, based on the original data provided by the input-line.
	 * assignment: {classes.Assignment} An instance of the classes.Assignment class.
	 * conversion: {Object} The corresponding definition, containing the numerables and identifiers for the assignment.
	 */
	processMetalLine( assignment, conversion ) {
		var mineral = undefined;
		var lineSymbols = assignment.data.substring( 0, assignment.data.indexOf( " is" ) ).split( " " );
		var lineValue = assignment.data.substring( assignment.data.indexOf( " is " ), assignment.data.indexOf( " Credit" ) ).replace( " is ", "" );
		var mineralAmount = 0;
		lineSymbols.forEach( ( lineSymbol, index )=> {
			var siblingSymbol = this.symbols.find( ( symbol )=> ( symbol.identifier === lineSymbols[ index + 1 ] ) );
			conversion = this.symbols.find( ( symbol )=> ( symbol.identifier === lineSymbol ) );
			/* If the conversion-amount is specified by the line, we're dealing with an amount of metals */
			if ( conversion !== null && conversion !== undefined ) {
				if ( conversion.isSubtractable( siblingSymbol ) ) {
					mineralAmount += ( ( mineralAmount - conversion.value.arabicNumeral ) );
				} else {
					mineralAmount += conversion.value.arabicNumeral;
				}
			}
			/* Else we're dealing with a metal */
			else {
				mineral = new classes.Mineral( lineSymbol, lineValue );
			}
		} );
		mineral.amount = mineralAmount;
		mineral.pricePerUnit = ( mineral.value / mineral.amount );
		this.symbols.push( mineral );
	};

	/*
	 * Iterates through the questions specified by the original input-data, generating a new classes.Answer
	 *     instance corresponding to each question asked, then triggers the calculate on the answer before adding it to this
	 *     objects' answers variable.
	 */
	processQuestions() {
		this.questions.forEach( ( question )=> {
			var answer = new classes.Answer();
			var lineSymbols = question.data.substring( question.data.indexOf( "is " ), question.data.indexOf( "?" ) ).replace( "is ", "" ).split( " " );

			lineSymbols.forEach( ( lineSymbol )=> {
				var symbol = undefined;
				if ( lineSymbol !== null && lineSymbol !== undefined && lineSymbol !== "" ) {
					var symbol = this.symbols.find( ( knownSymbol )=> ( knownSymbol.identifier === lineSymbol ) );
					answer.symbols.push( symbol );
				}
			} );
			answer.calculateResult();
			this.answers.push( answer );
		} );
	};
};

module.exports = Controller;