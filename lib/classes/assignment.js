
class Assignment extends classes.base.Line {
	constructor( data ) {
		super( data, "ASSIGNMENT" );
	};
};

module.exports = Assignment;