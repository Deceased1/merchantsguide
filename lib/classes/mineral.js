
class Mineral {
	constructor( identifier, value ) {
		this.identifier = identifier;
		this.value = value;
		this.pricePerUnit = undefined;
		this.amount = undefined;
	};
};

module.exports = Mineral;