
module.exports = Object.freeze( [ {
	romanNumeral: "I",
	arabicNumeral: 1
}, {
	romanNumeral: "V",
	arabicNumeral: 5
}, {
	romanNumeral: "X",
	arabicNumeral: 10
}, {
	romanNumeral: "L",
	arabicNumeral: 50
}, {
	romanNumeral: "C",
	arabicNumeral: 100
}, {
	romanNumeral: "D",
	arabicNumeral: 500
}, {
	romanNumeral: "M",
	arabicNumeral: 1000
} ] );