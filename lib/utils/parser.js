
class Parser {

	/*
	 * Determines whether an input-line is a question or an assignment, based on
	 *     whether the line ends with a question-mark or not.
	 * line: {String} A string either assigning values to symbols or metals ( else the system will not understand it... ).
	 * return {classes.Line} Returns a classes.Line instance, whether it is a question or an assignment.
	 */
	parseLine( line ) {
		if ( line === null || line === undefined ) {
			throw new TypeError( "line parameter is undefined" );
		}
		var result = undefined;
		if ( line.endsWith( "?" ) ) {
			result = new classes.Question( line );
		} else {
			result = new classes.Assignment( line );
		}

		if ( verbose === true ) {
			console.log( `LineType is ${ result.type }` );
		}
		return result;
	};
};

module.exports = Parser;