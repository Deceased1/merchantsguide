
const fs_module = require( "fs" );

const file_path = process.argv[ 2 ];

/*
 * Verifies that a file exists based on the FileSystem Stats provided by the
 *     underlying node-filesystem-module.
 * filePath: {String} The location where the file is located.
 * return {fs_module.Stats} Returns the FileSystem Stats for the file at the given location.
 */
function verifyFile( filePath ) {
	return new Promise( async ( resolve, reject )=> {
		fs_module.lstat( filePath, ( err, stat )=> {
			if ( err !== null && err !== undefined ) {
				reject( err );
			}
			resolve( stat );
		} );
	} );
}

/*
 * Reads the contents of a file as a byte-stream.
 * filePath: {String} The location where the file is located.
 * return {Buffer} Returns a Buffer containing the data of the file.
 */
function readFile( filePath ) {
	return new Promise( async ( resolve, reject )=> {
		fs_module.readFile( file_path, ( err, data )=> {
			if ( err !== null && err !== undefined ) {
				reject( err );
			}
			resolve( data.toString() );
		} );
	} );
};

/*
 * Initiates the process of reading a file.
 */
async function main( filePath ) {
	try {
		await verifyFile( filePath );
		console.log( await readFile( filePath ) );
		process.exit( 0 );
	} catch( err ) {
		console.log( err );
		process.exit( 1 );
	}
};

main( file_path );