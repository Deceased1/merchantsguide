
global.controllers = {};
global.classes = { base: {} };
global.definitions = {};
global.utils = {};

/* Definitions */
definitions.ConversionDefinition = require( "./definitions/conversionDefinition" );

/* Classes */
classes.base.Controller = require( "./classes/base/controller" );
classes.base.Line = require( "./classes/base/line" );
classes.base.Symbol = require( "./classes/base/symbol" );
classes.Answer = require( "./classes/answer" );
classes.Assignment = require( "./classes/assignment" );
classes.Mineral = require( "./classes/mineral" );
classes.Question = require( "./classes/question" );

/* Utils */
utils.Parser = require( "./utils/parser" );

/* Controllers */
controllers.FileController = require( "./controllers/fileController" );
controllers.InteractiveController = require( "./controllers/interactiveController" );
controllers.LineController = require( "./controllers/lineController" );